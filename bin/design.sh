#!/usr/bin/env bash

#########################################
###### TERMINAL DESIGN
#########################################

# https://wiki.bash-hackers.org/scripting/terminalcodes
x.colorgrid()
{
        for a in 0 1 4 5 7; do
                echo "a=$a "
                for (( f=0; f<=9; f++ )) ; do
                        for (( b=0; b<=9; b++ )) ; do
                                #echo -ne "f=$f b=$b"
                                echo -ne "\\033[${a};3${f};4${b}m"
                                echo -ne "\\\\\\\\033[${a};3${f};4${b}m"
                                echo -ne "\\033[0m "
                        done
                echo
                done
                echo
        done
        echo
}

# Reset
reset="\033[0m"       # Text Reset

# Regular Colors
black="\033[0;30m"        # Black
red="\033[0;31m"          # Red
green="\033[0;32m"        # Green
yellow="\033[0;33m"       # Yellow
blue="\033[0;34m"         # Blue
purple="\033[0;35m"       # Purple
cyan="\033[0;36m"         # Cyan
white="\033[0;37m"        # White

# Bold
bold_black="\033[1;30;40m"       # Black
bold_red="\033[1;31;40m"         # Red
bold_green="\033[1;32;40m"       # Green
bold_yellow="\033[1;33;40m"      # Yellow
bold_blue="\033[1;34;40m"        # Blue
bold_purple="\033[1;35;40m"      # Purple
bold_cyan="\033[1;36;40m"        # Cyan
bold_white="\033[1;37;40m"       # White

# Underline
underline_red="\033[4;31;40m"         # Red
underline_green="\033[4;32;40m"       # Green
underline_yellow="\033[4;33;40m"      # Yellow
underline_blue="\033[4;34;40m"        # Blue
underline_purple="\033[4;35;40m"      # Purple
underline_cyan="\033[4;36;40m"        # Cyan
underline_white="\033[4;37;40m"       # White

# Background
back_red="\033[1;37;41m"         # Red
back_green="\033[0;30;42m"       # Green
back_yellow="\033[0;30;43m"      # Yellow
back_blue="\033[0;37;44m"        # Blue
back_purple="\033[0;30;45m"      # Purple
back_cyan="\033[0;30;46m"        # Cyan
back_white="\033[0;30;47m"       # White

# Various variables you might want for your PS1 prompt instead
time12h="\T"
time12a="\@"
newline="\n"
path_short="\w"
path_full="\W"
jobs="\j"



# enable this flag ONLY if you are working with an internal repository that doesn't have a valid certificate
# export GIT_SSL_NO_VERIFY=true
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"


### Login Header
### TODO: get system values
# hardware usage
# info about upgradable packages
# etc ..


### TODO: make it easy tweakable

prompt_color="$red"
info_color="$back_red"

xtsh-ascii(){
  echo -en " $prompt_color

                        ${blue}.::.
                     ${blue}.:'  .:
          ${yellow}xXXXXooo${blue}.:'   .:'
         ${yellow}XXXXooooooo${blue}  .:'
        ${yellow}xXXXoooooooo${blue}.:'
        ${yellow}XXXXooooo${blue}.:'
      ${blue}.:${yellow}xXXXooo${blue}.:'
    ${blue}.:'  ${yellow}xXX${blue}.:'
  ${blue}.:'    .:'
 ${blue}:'   .:'
 ${blue}'::''


$info_color IP:  $prompt_color
$(x.ip)
--------------------------------------
$(sensors)
--------------------------------------
$(python3 __MARKDOWNVIEWER.py ___TODO___.md)
--------------------------------------
$(last | head -n 15)
--------------------------------------
$(last | grep "still running")
"
}
