#!/usr/bin/env bash

#########################################
###### ALIASES
#########################################
### System Enhancements
alias l='grc ls -lh'
alias ll='grc ls -lah'
alias lg="ls -lah | grep $1"
alias nmap='grc nmap'
alias osupgrade="sudo grc apt update && sudo grc apt upgrade"
alias ip='grc ip'
alias authlog="echo ´list open connections´; read; sudo grc netstat -antup; echo ´show authlog´; read; sudo grc tail -f /var/log/auth.log"
### XBASH SHORTS
alias x.cl="/usr/bin/clear && xtsh-ascii"
alias x.ip="ip a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' && ip a | sed -e's/^.*inet6 \([^ ]*\)\/.*$/\1/;t;d'"
alias x.logoff="rm ~/.bash_history; history -c; logout"
alias x.update="cd ~/aliases && git pull && sleep 3 && cd ~ && source .bashrc"